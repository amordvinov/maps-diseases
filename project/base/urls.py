from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns("",
                   url(r'^$', include('project.apps.main.urls', namespace='main')),
                   #(r'^accounts/', include('allauth.urls')),

                   #admin
                   url(r'^admin/', include(admin.site.urls)),)+staticfiles_urlpatterns()